import jetpack from 'fs-jetpack';

var app;
if (process.type === 'renderer') {
    app = require('electron').remote.app;
} else {
    app = require('electron').app;
}
var appDir = jetpack.cwd(app.getAppPath());

var data = appDir.read('./data/data.json', 'json');

export default data;
