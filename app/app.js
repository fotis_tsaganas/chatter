import Vue from 'vue';
import aksjcbakjscb from './data';
var crypto = require('crypto');
const storage = require('electron-json-storage');
var openpgp = require('openpgp');
// var aksjcbakjscb = data;
var data;

new Vue({
  el: '#vueapp',
  created: function () {
    // this.init();
    // this.login();
    // this.genKeys();
  },
  data: {
    app: {
        logged_in: false,
        keys: false
    },
    user: {
        username: '',
        pub_key: '',
        pr_key: '',
        passphrase: ''
    },
    friends:[],
    storage_data:null
  },
  methods: {
    lock: function () {
      var app = require("remote").require('app'); // or from BrowserWindow: app = require("remote").require('app');
      var exec = require('child_process').exec;
      exec(process.execPath);
      app.quit();
    },
    init: function () {
      var self = this;
      storage.has('data', function(error, hasKey) {
          var cipher = crypto.createCipher('aes-256-ctr',self.user.passphrase);
          var crypted = cipher.update(JSON.stringify(aksjcbakjscb),'utf8','hex');
          crypted += cipher.final('hex');
          if (!hasKey) {
              console.log('First Time');
              storage.set('data', { data: crypted }, function(error) {
                  console.log(error);
                  self.login();
              });
          }else {
              storage.get('data', function(error, data) {
                  var decipher = crypto.createDecipher('aes-256-ctr',self.user.passphrase);
                  var dec = decipher.update(data.data,'hex','utf8');
                  dec += decipher.final('utf8');
                  try{
                      self.storage_data=JSON.parse(dec);
                      console.log(self.storage_data);
                      self.app.logged_in = true;
                      self.login();
                  }catch(e){
                      console.log("Wrong Password");
                      console.log(e);
                      // self.login();
                  }
              });
          }
      });

    },
    del: function () {
      storage.remove('data', function(error) {
          if (error) throw error;
      });
    },
    save: function () {

    },
    login: function () {
        var private_key = openpgp.key.readArmored(this.storage_data.user.pr_key).keys[0];
        var ckeck = private_key.decrypt(this.user.passphrase);
        console.log(ckeck);
        if (ckeck) {
            console.log('Good for key');
            this.getKeys();
            this.getFriends();
        }else {
            console.log('Not Good for key');
        }
    },
    getFriends: function () {
        this.friends = this.storage_data.friends;
    },
    getKeys: function () {
        this.user.username = this.storage_data.user.username;
        this.user.pub_key = this.storage_data.user.pub_key;
        this.user.pr_key = this.storage_data.user.pr_key;
    },
    genKeys: function () {
        var options = {
            userIds: [{ name:'Fotis Tsaganas', email:'fotisteipir@gmail.com' }],
            numBits: 1024,
            passphrase: '1234'
        };
        console.log(options);
        var self = this;
        openpgp.generateKey(options).then(function(key) {
            self.pr_key = key.privateKeyArmored;
            self.pub_key = key.publicKeyArmored;
        }, function(err) {
            console.log(err);
        });
    },
    decryptKey: function () {
          var priv_key_unlocked = openpgp.decryptKey(this.pr_key, this.passphrase);
          console.log(priv_key_unlocked);
          this.pr_key_unlocked = priv_key_unlocked;
    },
    encrypt_message: function () {
          var options, encrypted;

          options = {
            data: this.message,
            publicKeys: openpgp.key.readArmored(this.pub_key).keys[0]
          };
          var self = this;
          openpgp.encrypt(options).then(function(armored) {
              encrypted = armored;
              self.message = encrypted.data;
          }, function(err) {
              console.log(err);
          });
    },
    decrypt_message: function () {
          var options, encrypted;
          var prkey = openpgp.key.readArmored(this.pr_key).keys[0];
          prkey.decrypt(this.passphrase);
          options = {
            message: openpgp.message.readArmored(this.message),
            privateKey: prkey
          };
          var self = this;
          openpgp.decrypt(options).then(function(plaintext) {
              self.message = plaintext.data;
          }, function(err) {
              console.log(err);
          });
    }
 }
});






//
